#!/bin/bash


log_name=./install_log_$(date '+%Y-%m-%d_%H-%M-%S')
exec > >(tee $log_name) 2>&1



prompt()
{
    local RETURN_VAL=$1
    local PRINT_TEXT=$2
    local INVALID_CHARS=$3
    local TEXT_INFO=$4
    local EVAL_INFO=$5
    local DONT_SHOW=$6
    local INPUT

    while [ -z "$INPUT" ] || [[ $INPUT == "?" ]]; do

        printf "$PRINT_TEXT [q]=quit [?]=info: "

        if [ -z "$DONT_SHOW" ]; then
            read INPUT
        else
            printf "(hidden): "
            read -s INPUT
        fi


        if [[ $INPUT == "q" ]]; then
            echo "Aborted!"
            exit
        elif [[ $INPUT == "?" ]]; then
            printf "\n$TEXT_INFO\n"
            eval $EVAL_INFO
        elif [ -z "$INPUT" ] || [[ $INPUT = *[$INVALID_CHARS]* ]]; then
            printf "\ninput is invalid"
            INPUT=""
        else
            eval $RETURN_VAL="'$INPUT'"
        fi

        printf "\n"

    done
}

printf "\nVOID INSTALL SCRIPT\n\n"

prompt KEYBOARD_MAP \
       "Choose your keymap"\
       "$(printf ' \n\t')"\
       "example: de | fr | us"\
       "ls -R /usr/share/kbd/keymaps | grep .gz | sort | awk -F. '{print $1}' | less"

#loadkeys "$KEYBOARD_MAP"


prompt DISK \
       "Choose disk to install void"\
       "$(printf ' \n\t')"\
       "example: sda | sdb"\
       "printf '\n' && lsblk | grep NAME && lsblk | grep disk"

prompt CRYPT_NAME \
       "Choose a name for the LVM volume group"\
       "$(printf ' \n\t')"\
       "example: voidvm | crypt"

prompt HOSTNAME \
       "Choose a hostname"\
       "$(printf ' \n\t')"\
       "example: voidbox | thinkpad"


PW1="---"
PW2="+++"

while [[ $PW1 != $PW2 ]]; do


    prompt PW1 \
           "Choose a root/crypt password" ""\
           "example: mY_-V3rY-Str0nG_pW!!?" ""\
           "dont show"

    prompt PW2 \
           "Retype root/crypt password" ""\
           "example: mY_-V3rY-Str0nG_pW!!?" ""\
           "dont show"

    if [[ $PW1 != $PW2 ]]; then
        printf "\npasswords do not match --> try again\n"
    else
        PASSWORD_ROOT=$PW2
    fi

    printf "\n"

done




while [ -z $INSTALL_YES ]; do

    printf "Are you shure you want to install [y]=yes [q]=quit [?]=info: "
    read INSTALL_YES

    case "$INSTALL_YES" in
    "y")
        break
        ;;
    "q")
        exit
        ;;
    "?")
        printf "\n"
        echo "Disk to install: $DISK"
        echo "LVM volume name: $CRYPT_NAME"
        echo "Hostname:        $HOSTNAME"
        printf "\n"
        INSTALL_YES=""
        ;;
     *)
        INSTALL_YES=""
        ;;
  esac

done


printf "\nformat disk\n"


sfdisk --force -W always /dev/${DISK} << EOF
	label: gpt
	,500M,U
	;
EOF

sleep 2

partx /dev/${DISK}

printf "\nencrypting disk\n"

cryptsetup luksFormat --type luks1 /dev/${DISK}2 << EOF
$PASSWORD_ROOT
EOF

cryptsetup luksOpen /dev/${DISK}2 $CRYPT_NAME << EOF
$PASSWORD_ROOT
EOF

printf "\ncreate logical volumes\n"

vgcreate $CRYPT_NAME /dev/mapper/$CRYPT_NAME

lvcreate --name root -L 10G $CRYPT_NAME
lvcreate --name swap -L "$(($(free -m | grep Mem: | awk '{printf $2}') * 2))m" $CRYPT_NAME
lvcreate --name home -l 100%FREE $CRYPT_NAME


printf "\ncreate the filesystems\n"

mkfs.ext4 -L root /dev/$CRYPT_NAME/root
mkfs.ext4 -L home /dev/$CRYPT_NAME/home
mkswap /dev/$CRYPT_NAME/swap
mkfs.vfat /dev/${DISK}1


printf "\nmount the filesystems\n"

mount /dev/$CRYPT_NAME/root /mnt

for dir in dev proc sys run; do
mkdir -p /mnt/$dir
mount --rbind /$dir /mnt/$dir
mount --make-rslave /mnt/$dir
done

mkdir -p /mnt/home
mount /dev/$CRYPT_NAME/home /mnt/home

mkdir -p /mnt/boot/efi
mount /dev/${DISK}1 /mnt/boot/efi

#DISK_ONE_UUID=$(blkid -o value -s UUID /dev/${DISK}1)
#DISK_TWO_UUID=$(blkid -o value -s UUID /dev/${DISK}2)

DISK_ONE_UUID=$(lsblk -f /dev/${DISK}1 | grep ${DISK}1 | awk '{print $4}')
DISK_TWO_UUID=$(lsblk -f /dev/${DISK}2 | grep ${DISK}2 | awk '{print $4}')


printf "\ninstalling void to disk\n"

echo "y" | xbps-install -Sy -R https://alpha.de.repo.voidlinux.org/current -r /mnt base-system cryptsetup grub-x86_64-efi lvm2 micro git


printf "\nchroot to void and setup\n"

chroot /mnt /bin/bash << EOT
chown root:root /
chmod 755 /
#ln -s /hostrun /run
printf "$PASSWORD_ROOT\n$PASSWORD_ROOT\n" | passwd root
echo voidvm > /etc/hostname
echo "LANG=en_US.UTF-8" > /etc/locale.conf
echo "en_US.UTF-8 UTF-8" >> /etc/default/libc-locales
xbps-reconfigure -f glibc-locales


printf "\nwrite fstab\n"

cat > /etc/fstab << EOF
# <file system>	   		<dir>       <type>      <options>               <dump>  <pass>
tmpfs      				/tmp        tmpfs       defaults,nosuid,nodev   0       0
UUID=${DISK_ONE_UUID}	/boot/efi   vfat	    defaults	            0	    2
/dev/$CRYPT_NAME/root   /           ext4        defaults                0       1
/dev/$CRYPT_NAME/home   /home       ext4        defaults                0       2
/dev/$CRYPT_NAME/swap   swap        swap        defaults                0       1
EOF

cat /etc/fstab


printf "\nwrite default grub\n"

cat > /etc/default/grub << EOF
#
# Configuration file for GRUB.
#
GRUB_DEFAULT=0
GRUB_TIMEOUT=5
GRUB_DISTRIBUTOR="Void"
GRUB_ENABLE_CRYPTODISK=y
GRUB_CMDLINE_LINUX_DEFAULT="loglevel=4 slub_debug=P page_poison=1 rd.lvm.vg=${CRYPT_NAME} rd.luks.uuid=${DISK_TWO_UUID}"
EOF

cat /etc/default/grub

printf "\ngenerate keyfile\n"

dd bs=512 count=4 if=/dev/urandom of=/key

cryptsetup luksAddKey /dev/${DISK}2 /key << EOF
$PASSWORD_ROOT
EOF

chmod 000 /key


printf "\nadd keyfile to crypttab\n"

cat > /etc/crypttab << EOF
$CRYPT_NAME   /dev/${DISK}2   /key   luks
EOF

cat /etc/crypttab

printf "\nadd keyfile to dracut.conf\n"

cat > /etc/dracut.conf.d/10-crypt.conf << EOF
install_items+=" /key /etc/crypttab "
EOF

cat /etc/dracut.conf.d/10-crypt.conf

printf "\ninstalling grub\n"

grub-install -v --target=x86_64-efi --efi-directory=/boot/efi --bootloader-id=Void --recheck

xbps-reconfigure -fa


exit

EOT

# umount -R /mnt



prompt SHUTDOWN \
       "Installation finished successfull. Reboot? [y]=yes "\
       "$(printf ' \n\t')"

if [[ $SHUTDOWN = "y" ]]; then
    shutdown -h now
fi



#echo $KEYBOARD_MAP
#echo $DISK
#echo $HOSTNAME
#echo $CRYPT_NAME
#echo $PASSWORD_ROOT
